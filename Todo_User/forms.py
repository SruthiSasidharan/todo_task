from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import *


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()


class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ["first_name", "email", "username", "password1", "password2"]

class TodoCreateForm(forms.ModelForm):
    class Meta:
        model = Todo
        fields = "__all__"

class TodoUpdateForm(forms.ModelForm):
    class Meta:
        model=Todo
        fields='__all__'
