from django.db import models


class Todo(models.Model):
    Title = models.CharField(max_length=50)
    Description = models.CharField(max_length=150)
    Created_On = models.DateTimeField(auto_now_add=True)
    choices = (("In Progress", "In Progress"),
               ("Pending", "Pending"),
               ("Completed","Completed"))

    status = models.CharField(max_length=50, choices=choices, default="In Progress")
    user = models.CharField(max_length=50)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user
