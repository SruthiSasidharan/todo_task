from django.urls import path
from .views import *
urlpatterns = [
    path("register",registration,name="register"),
    path("login",django_login,name="login"),
    path("create",Create_todo,name="create"),
    path("list",list_all_todos,name="list"),
    path("update/<int:id>",update_todo,name="update"),
    path("delete/<int:id>",delete_todo,name="delete")
]