from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from .forms import *


def django_login(request):
    form = LoginForm()
    context = {}
    context["form"] = form
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                return render(request, "todo.html")
            else:
                context["form"] = form
                return render(request, "login.html", context)

    return render(request, "login.html", context)


def registration(request):
    form = UserRegistrationForm()
    context = {}
    context["form"] = form
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect("login")
        else:
            context["form"] = form
            return render(request, "register.html", context)

    return render(request, "register.html", context)


def Create_todo(request):
    context = {}
    form = TodoCreateForm(initial={"user": request.user})
    context["form"] = form
    if request.method == "POST":
        form = TodoCreateForm(request.POST)
        if form.is_valid():
            Title = form.cleaned_data.get("Title")
            Description = form.cleaned_data.get("Description")
            Created_On = form.cleaned_data.get("Created_On")
            modified_at= form.cleaned_data.get("modified_at")
            status = form.cleaned_data.get("status")
            user = form.cleaned_data.get("user")

            td = Todo(Title=Title, Description=Description, Created_On=Created_On, status=status,modified_at=modified_at, user=user)
            td.save()
            print("saved")
            return redirect("list")
    return render(request, "todo.html", context)


def list_all_todos(request, *args, **kwargs):
    todos = Todo.objects.filter(user=request.user)
    context = {}
    context["todos"] = todos
    return render(request, "list.html", context)


def update_todo(request, *args, **kwargs):
    id = kwargs.get("id")
    td = Todo.objects.get(id=id)
    form = TodoUpdateForm(instance=td)
    context = {}
    context["form"] = form
    if request.method == "POST":
        form = TodoUpdateForm(instance=td, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect("list")
    return render(request, "update.html", context)


def delete_todo(request, *args, **kwargs):
    id = kwargs.get("id")
    td = Todo.objects.get(id=id)
    td.delete()
    return redirect("list")
