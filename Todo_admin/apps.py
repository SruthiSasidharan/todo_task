from django.apps import AppConfig


class TodoAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Todo_admin'
