from rest_framework import filters
from rest_framework.generics import ListAPIView

from .serializers import *


class AllTodos(ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['^Title', '^status']